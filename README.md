##Autowire 기본

1. *디렉토리 구성*
```
./src/
└── main
    ├── java
    │   └── autowire
    │       ├── Instrumentalist.java
    │       ├── Instrument.java
    │       ├── Main.java
    │       ├── Performer.java
    │       └── Saxophone.java
    └── resources
        └── autowire.xml

```
---
2. *주의 및 참고*
	+ XML의 설정 간소화를 위해 bean의 오토와이어링 기능을 제공한다.
	+ byName 변수의 이름
	+ byType 변수의 형태
	+ constructor 변수의 형태
	+ autodetect - 생성자 -> 타입 -> 이름 순서로 자동 와이어링 한다.
  + autowire.xml 을 참고하여 byName의 성격을 확인한다.
---


