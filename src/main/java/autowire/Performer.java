package autowire;

public interface Performer {
	public void perform();
}
