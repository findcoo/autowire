package autowire;


public interface Instrument {
	public void play();
}
